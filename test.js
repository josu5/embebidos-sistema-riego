var http = require('http');
var path = require('path');
var url = require('url');
var fs = require('fs');
http.createServer(function (request, response) {
	
	var filePath = '.' + request.url;
	if (filePath == './')
		filePath = './index.html';

	var url_request = url.parse(request.url).pathname; 
	var tmp  = url_request.lastIndexOf(".");
	var extension  = url_request.substring((tmp + 1));

	path.exists(filePath, function(exists) {
	
		if (exists) {
			fs.readFile(filePath, function(error, content) {
				if (error) {
					response.writeHead(500);
					response.end();
				}
				else {
					if (extension === 'html') response.writeHeader(200, {"Content-Type": 'text/html'});
			          else if (extension === 'htm') response.writeHeader(200, {"Content-Type": 'text/html'});
			          else if (extension === 'css') response.writeHeader(200, {"Content-Type": 'text/css'});
			          else if (extension === 'js') response.writeHeader(200, {"Content-Type": 'text/javascript'});
			          else if (extension === 'png') response.writeHeader(200, {"Content-Type": 'image/png'});
			          else if (extension === 'jpg') response.writeHeader(200, {"Content-Type": 'image/jpg'});
			          else if (extension === 'jpeg') response.writeHeader(200, {"Content-Type": 'image/jpeg'});
			          else {console.log(url_request)};
			          response.end(content, 'utf-8');
				}
			});
		}
		else {
			response.writeHead(404);
			response.end();
		}
	});
	
}).listen(8000);